angular.module('PokemonApp')
  .factory('BerriesService', function ($resource, $http) {

    return $resource('http://api.backendless.com/v1/data/berry', {}, {
      query: {
        isArray: true,
        transformResponse: function (responseData) {
          //console.log(angular.fromJson(responseData));

          return angular.fromJson(responseData).data;
        }
      }});
});
